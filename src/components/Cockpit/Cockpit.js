import React, { useContext } from 'react';
import './Cockpit.css';
import AuthContext from '../../context/auth-context';

const Cockpit = props => {
  const { login } = useContext(AuthContext);
  const { click, persons, showPersons } = props;

  const style = {
    backgroundColor: !showPersons ? 'green' : 'red',
    color: 'white',
    border: '1px black solid',
    padding: '8px',
    borderRadius: '5px',
  };

  const classes = [];
  if (persons.length.length <= 2) {
    classes.push('red');
  }
  if (persons.length.length <= 1) {
    classes.push('bold');
  }

  return (
    <div>
      <h1>The people Show</h1>
      <h2 className={classes.join(' ')}>We got some people</h2>
      <button style={style} onClick={click}>
        Swtich data
      </button>

      <button style={style} onClick={login}>
        Log In!
      </button>
    </div>
  );
};

export default Cockpit;
