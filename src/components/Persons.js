import React from 'react';
import Person from './Person/Person';
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';

const Persons = props => {
  const { persons, click, change } = props;

  return persons.map((person, index) => {
    return (
      <ErrorBoundary key={person.id}>
        <Person
          name={person.name}
          age={person.age}
          change={event => change(event, index)}
          click={() => click(index)}
        />
      </ErrorBoundary>
    );
  });
};

export default Persons;
