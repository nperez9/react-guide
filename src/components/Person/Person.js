import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import AuthContext from '../../context/auth-context';
// React not encapsulate styles
import './Person.css';

// in this way whe can reciebe the props
const person = props => {
  const inputRef = useRef(null);
  const { name, age, children, click, change } = props;

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  return (
    <div className="Person">
      <AuthContext.Consumer>
        {context => (context.authenticated ? <p>Autentitcated</p> : <p>Use the login</p>)}
      </AuthContext.Consumer>
      <p onClick={click}>
        {' '}
        I Am a {name}! I got {age} years old
      </p>
      <p>{children}</p>
      <input type="text" ref={inputRef} onChange={change} value={name} />
    </div>
  );
};

person.PropTypes = {
  click: PropTypes.func,
  name: PropTypes.string,
  age: PropTypes.age,
  change: PropTypes.func,
};

export default person;
