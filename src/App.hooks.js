import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person';
// is the same tan APP but with react hooks


const App = () =>  {
  const [ personState, setPersonState ] = useState({
    persons: [
      { name: "nikolai", age: 24},
      { name: "tanita", age: 22, data: "sister of cortiz"},
      { name: "pedro", age: 33}
    ],
    other: 'other states'
  });


  const switchDataHandler = () => {
    // this method do NOT merge the state like class based compoonent
    setPersonState({
      ...personState,
      persons: [
        { name: "asdad", age: 24},
        { name: "iach", age: 24, data: "sister of cortiz"},
        { name: "pedro", age: 33}
      ]
    })
  }

  return (
    <div className="App">
      <h1> Soy el react </h1>
      <button onClick={switchDataHandler}>Swtich data</button>
      <Person name={personState.persons[0].name} age={personState.persons[0].age}/>
      <Person name={personState.persons[1].name} age={personState.persons[1].age} >{personState.persons[1].data}</Person>
      <Person name={personState.persons[2].name} age={personState.persons[2].age} /> 
    </div>
  );
}

export default App;
