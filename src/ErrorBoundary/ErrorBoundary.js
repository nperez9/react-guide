import React, { Component } from 'react';

// wrap the error, use it only the component will fail
class ErrorBoundary extends Component {
  state = {
    hasError: false,
    errorMessage: '',
  };

  componentDidCatch(error) {
    this.setState({
      hasError: true,
      errorMessage: error.message,
    });
  }

  // renders only in prod modes
  render() {
    if (this.state.hasError) {
      return <h1>{this.state.errorMessage}</h1>;
    } else {
      return this.props.children;
    }
  }
}

export default ErrorBoundary;
