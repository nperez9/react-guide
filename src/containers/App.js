import React, { Component } from 'react';
import './App.css';
import Persons from '../components/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import withClass from '../hoc/withClass';
import Auxiliary from '../hoc/Auxiliary';
import AuthContext from '../context/auth-context';

// Capital elements are for custom components
class App extends Component {
  // reserved word, only for the state
  state = {
    persons: [
      { id: 1, name: 'nikolai', age: 24 },
      { id: 2, name: 'cortiz', age: 22, data: 'haaaaa' },
      { id: 3, name: 'salome', age: 30 },
    ],
    showPersons: false,
    changeCounter: 0,
    isAuthenticated: false,
  };

  // handler is for its called by event
  changeNameHandler = (event, index) => {
    const person = this.state.persons[index];
    person.name = event.target.value;

    // the state should be inmutable
    const persons = [...this.state.persons];
    persons[index] = person;

    this.setState((prevState, props) => {
      return {
        persons,
        changeCounter: ++prevState.changeCounter,
      };
    });
  };

  loginHandler = () => {
    this.setState({ isAuthenticated: true });
  };

  showPersonsHandler = () => {
    const showPerson = this.state.showPersons;
    // merges the state
    this.setState({ showPersons: !showPerson });
  };

  deletePersonHandler = personIndex => {
    // The state MUST be inmutable
    const persons = this.state.persons.map(p => p);
    persons.splice(personIndex, 1);
    this.setState({ persons });
  };

  // this executes with every state change
  render() {
    let persons = null;
    if (this.state.showPersons) {
      // handles everything when states change

      persons = (
        <Persons
          persons={this.state.persons}
          click={this.deletePersonHandler}
          change={this.changeNameHandler}
        ></Persons>
      );
    }

    return (
      <Auxiliary>
        <AuthContext.Provider
          value={{
            authenticated: this.state.isAuthenticated,
            login: this.loginHandler,
          }}
        >
          <Cockpit
            persons={this.state.persons}
            showPersons={this.state.showPersons}
            click={this.showPersonsHandler}
          />
          {persons}
        </AuthContext.Provider>
      </Auxiliary>
    );
  }
}

export default withClass(App, 'App');
