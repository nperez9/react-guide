import React from 'react';

// NOTE: Initialization is recomended for a better autocompletation
const authContext = React.createContext({
  authenticated: false,
  login: () => {},
});

export default authContext;
